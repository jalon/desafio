## Teste – Capgemini

# Pre-requisitos
``` Docker ```
``` Java 11 ```

# Instalação
``` docker build -t challenge . ```<br>

# Start
``` docker run -p 8080:8080 challenge ```
