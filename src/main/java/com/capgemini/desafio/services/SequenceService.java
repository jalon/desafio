package com.capgemini.desafio.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.desafio.entities.Letter;
import com.capgemini.desafio.entities.Sequences;
import com.capgemini.desafio.repositories.SequenceRepository;
import com.google.gson.Gson;

@Service
public class SequenceService {

    @Autowired
    private SequenceRepository repository;

    public Map<String, Integer> getStats(){
        Integer sequencesValids = repository.findByValid(true).size();
        Integer sequencesInvalids = repository.findByValid(false).size();

        Map<String, Integer> response = new HashMap<>();
        response.put("count_valid", sequencesValids);
        response.put("count_invalid", sequencesInvalids);
        response.put("ratio", (sequencesValids + sequencesInvalids) > 0 ? sequencesValids / (sequencesValids + sequencesInvalids) : 0);

        return response;
    }

    public Map<String, Boolean> analyse(Letter payload) {
        String[] letters = payload.getLetters();
        int valids = 0;
        Boolean value = false;

        for (int line = 0; line < letters.length; line++) {
            valids += analyseOneToOne(letters, line);
        }

        Map<String, Boolean> response = new HashMap<>();

        if(valids > 2) {
            value = true;
        } else {
            value = false;
        }

        Gson gson = new Gson();
        Sequences sequenceObj = new Sequences(null, gson.toJson(letters), value);
        repository.save(sequenceObj); 

        response.put("is_valid", value);

        return response;
    }

    private Integer analyseOneToOne(String[] letters, int line) {
        String palavra = letters[line].trim();
        int countValid = 0;

        for (int charIndex = 0; charIndex < palavra.length(); charIndex++) {
            char letra = palavra.charAt(charIndex);
            Boolean result = compare(letra, letters, line, charIndex);

            if(result == true) countValid++;
        }
        
        return countValid;
    }

    private Boolean compareVertical(char letra, String[] letters, int line, int charIndex, int countValid){
        if(line <= letters.length) {
            if(letra == letters[line+1].trim().charAt(charIndex)) {
                countValid++;
                if(countValid < 3) {
                    return compareVertical(letra, letters, (line+1), charIndex, countValid);
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    private Boolean compareHorizontal(char letra, String[] letters, int line, int charIndex, int countValid){
        if(charIndex <= letters[0].trim().length()) {
            if(letra == letters[line].trim().charAt(charIndex+1)) {
                countValid++;
                if(countValid < 3) {
                    return compareHorizontal(letra, letters, line, (charIndex+1), countValid);
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    private Boolean compareDiagonalEsquerda(char letra, String[] letters, int line, int charIndex, int countValid){        
        if((line >= 0 && charIndex >= 0) && (line <= letters.length && charIndex <= letters[0].trim().length())) {
            if(letra == letters[line+1].trim().charAt(charIndex-1)) {
                countValid++;
                if(countValid < 3) {
                    return compareDiagonalEsquerda(letra, letters, (line+1), (charIndex-1), countValid);
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    private Boolean compareDiagonalDireita(char letra, String[] letters, int line, int charIndex, int countValid){
        if((line >= 0 && charIndex >= 0) && (line <= letters.length && charIndex <= letters[0].trim().length())) {
            if(letra == letters[line+1].trim().charAt(charIndex+1)) {
                countValid++;
                if(countValid < 3) {
                    return compareDiagonalDireita(letra, letters, line+1, charIndex+1, countValid);
                } else {
                    return true;
                }
            }
        }

        return false;
    }


    private Boolean compare(char letra, String[] letters, int line, int charIndex) {
        Boolean response = false;        

        if(response != true && 
            line < letters.length - 3
        )   response = compareVertical(letra, letters, line, charIndex, 0);

        if(response != true && 
            charIndex < letters[0].trim().length() - 3
        )   response = compareHorizontal(letra, letters, line, charIndex, 0);

        if(response != true && 
            line < (letters.length - 3) && charIndex >= (letters[0].trim().length() - 3)
        )   response = compareDiagonalEsquerda(letra, letters, line, charIndex, 0);

        if(response != true && 
            line < (letters.length - 3) && charIndex < (letters[0].trim().length() - 3)
        )   response = compareDiagonalDireita(letra, letters, line, charIndex, 0);

        return response;
    }
}
