package com.capgemini.desafio.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.desafio.entities.Letter;
import com.capgemini.desafio.services.SequenceService;

@RestController
@RequestMapping(value = "/")
public class SequenceController {
    
    @Autowired
    private SequenceService service;

    @PostMapping(path = "sequence")
    public ResponseEntity<Object> analyse(@RequestBody Letter payload) {
        Map<String, Boolean> result = service.analyse(payload);

        return ResponseEntity.ok().body(result);
    }

    @GetMapping(path = "stats")
    public ResponseEntity<Object> stats(){
        Map<String, Integer> result = service.getStats();

        return ResponseEntity.ok().body(result);
    }
}
