package com.capgemini.desafio.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.desafio.entities.Sequences;

public interface SequenceRepository extends JpaRepository<Sequences, Integer>{
    List<Sequences> findByValid(Boolean valid);
}
