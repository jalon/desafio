package com.capgemini.desafio.entities;

public class Letter {
    private String[] letters;

    public String[] getLetters() {
        return letters;
    }

    public void setLetters(String[] letters) {
        this.letters = letters;
    }
}
