package com.capgemini.desafio.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sequences")
public class Sequences {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String seqJson;
    private Boolean valid;

    public Sequences(){

    }

    public Sequences(Integer id, String seqJson, Boolean valid) {
        this.id = id;
        this.seqJson = seqJson;
        this.valid = valid;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSeqJson() {
        return seqJson;
    }
    public void setSeqJson(String seqJson) {
        this.seqJson = seqJson;
    }
    public Boolean getValid() {
        return valid;
    }
    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
